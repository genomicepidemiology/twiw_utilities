#!/usr/bin/env python3.7

import sys
import os
from glob import glob
import mysql.connector
from mysql.connector import errorcode
import argparse
import configparser
sys.path.append(os.path.dirname(sys.argv[0]))
from mysql_access import DATABASE_ACCESS

class SEQUENCING_RUN_PARSER:
    """Parse QC and typing info for twiw sequencing runs"""

    def __init__(self, assembly_filename):
        # with .fa
        self.assembly_filename = assembly_filename
        self.sequence_name = assembly_filename.rsplit("_",2)[0]
        self.sample_id = self.sequence_name.split("_")[2]

    def parse_qc_txt(self, qc_folder):
        assembly_name = self.assembly_filename.rsplit(".")[0]
        self.qc_txt_path = os.path.join(qc_folder, f"{assembly_name}.qc.txt")
        self.qc_stats = { "bases_sequenced": "",
        "qual_bases_mc": "",
        "qual_bases_percentage": "",
        "reads": "",
        "qual_reads": "",
        "qual_reads_percentage": "",
        "most_common_adapter_count": "",
        "second_most_common_adapter_count": "",
        "other_adapters_count": "",
        "placeholder": "",
        "n50": "",
        "no_ctgs_min_500": "",
        "longest": "",
        "assembly_size_bp": ""
        }

        try:
            qc_input = open(self.qc_txt_path, "r")
        except Exception as e:
            print(f"Error: {self.qc_txt_path} not found", file=sys.stderr)
        else:
            # it's one line
            line = qc_input.readline()
            # DTU_2021_1002393_1_SI_TUR_ORT_040_R1_001.fq.gz	371	247	66.67%	2546808	1978272	77.68%	643	19	76		49359	219	142107	5023839
            value_cols = line.strip().split("\t")[1:]
            # they are in the same order as the dict keys, python3.7 preserves insertion order
            for i, colname in enumerate(list(self.qc_stats)):
                self.qc_stats[colname] = value_cols[i].strip("%")
            qc_input.close()
            raw_coverage = int(self.qc_stats["qual_bases_mc"]) * 1000000 / float(self.qc_stats["assembly_size_bp"])
            self.qc_stats["coverage"] = f"{raw_coverage:.2f}"
        self.qc_stats.pop("placeholder", None)
        return

    def parse_kf_spa(self, kf_folder):
        # kf_folder is the lowest level folder
        self.kf_spa_path = os.path.join(kf_folder, f"kmerresults_{self.assembly_filename}.spa")
        self.kf_results = { "kmerfinder_main_hit": "",
        "kmerfinder_main_hit_percentage": "",
        "other_content_1": "",
        "other_content_1_query_coverage": "",
        "other_content_2": "",
        "other_content_2_query_coverage": "",
        "other_content_3": "",
        "other_content_3_query_coverage": ""
        }

        try:
            kf_input = open(self.kf_spa_path, "r")
        except Exception as e:
            print(f"Error: {self.kf_spa_path} not found", file=sys.stderr)
        else:
            # parse all lines, gather max tot_query_coverage for each species
            kf_species = {}
            _ = kf_input.readline()
            for line in kf_input:
                cols = line.strip().split("\t")
                hit_species = " ".join(cols[0].split(" ")[1:3])
                try:
                    hit_tot_query_coverage = float(cols[8])
                except TypeError as e:
                    sys.exit("Error: spa file formatting error")
                else:
                    if kf_species.get(hit_species) is None:
                        kf_species[hit_species] = hit_tot_query_coverage
                    else:
                        if hit_tot_query_coverage > kf_species[hit_species]:
                            # replace with new coverage
                            kf_species[hit_species] = hit_tot_query_coverage
            kf_input.close()
            # top 4 species
            sorted_cov = sorted(list(kf_species.values()), reverse=True)[:4]
            inverz_kf_species = {kf_species.get(x):x for x in kf_species}
            no_hits = len(sorted_cov)
            for i, param in enumerate(list(self.kf_results)):
                hit_pos = i // 2
                if hit_pos < no_hits:
                    if i % 2:
                        # coverage, back to str
                        self.kf_results[param] = f"{sorted_cov[hit_pos]:.2f}"
                    else:
                        # taxonomy
                        self.kf_results[param] = inverz_kf_species[sorted_cov[hit_pos]]
        return

    def parse_rmlst_txt(self, rmlst_folder):
        # kf_folder is the lowest level folder
        self.rmlst_results_path = os.path.join(rmlst_folder, f"{self.assembly_filename}.txt")
        self.rmlst_results = { "rmlst_species": "",
        "rank": "",
        "support": "",
        "rmlst_species_hit2": "",
        "rank_hit2": "",
        "support_hit2": ""
        }

        try:
            rmlst_input = open(self.rmlst_results_path, "r")
        except Exception as e:
            print(f"Error: {self.rmlst_results_path} not found", file=sys.stderr)
        else:
            # DTU_2021_1003037_1_SI_PRY_ASU_HSWI_017_R1_001.fa
            # Rank: SPECIES
            # Taxon:Enterobacter roggenkampii
            # Support:65%
            # TaxonomyProteobacteria > Gammaproteobacteria > Enterobacterales > Enterobacteriaceae > Enterobacter > Enterobacter roggenkampii
            # parse all hits
            rmlst_taxa = []
            hit = -1
            for line in rmlst_input:
                if line.startswith("Rank:"):
                    hit += 1
                    rmlst_taxa.append(["","",""])
                    rmlst_taxa[hit][1] = line.strip().split(":")[1].strip()
                elif line.startswith("Taxon:"):
                    rmlst_taxa[hit][0] = line.strip().split(":",1)[1].strip()
                    # avoid duplicates
                    if hit != 0 and rmlst_taxa[0][0] == rmlst_taxa[hit][0]:
                        rmlst_taxa.pop(hit)
                        break
                elif line.startswith("Support:"):
                    rmlst_taxa[hit][2] = line.strip().split(":")[1][:-1].strip()
            # top 2 taxa
            no_hits = len(rmlst_taxa)
            for i, param in enumerate(list(self.rmlst_results)):
                hit_pos = i // 3
                if hit_pos < no_hits:
                    if i % 3 == 0:
                        # taxonomy
                        self.rmlst_results[param] = rmlst_taxa[hit_pos][0]
                    elif i % 3 == 1:
                        # rank
                        self.rmlst_results[param] = rmlst_taxa[hit_pos][1]
                    else:
                        # support
                        self.rmlst_results[param] = rmlst_taxa[hit_pos][2]
        return

    def fill_column(self, varname, dictentry):
        try:
            setattr(self, varname, " ".join(dictentry.values()))
        except TypeError:
            for k in dictentry.keys():
                dictentry[k] = str(dictentry.get(k))
            setattr(self, varname, " ".join(dictentry.values()))
        except AttributeError:
            setattr(self, varname, "")

    def evaluate_typing(self):
        # thresholds
        KF_CONTAMINATION_MIN = 80.0
        KF_OTHER_HITS_MAX = 80.0
        RMLST_CONTAMINATION_MIN = 90
        RMLST_CLEAR_CONTAMINATION_MIN = 95

        self.possible_contamination_alert = ""
        self.bioinformaic_concordance = self.rmlst_results["rmlst_species"]
        self.concluding_id = ""
        # do the typing methods agree?
        if self.rmlst_results["rmlst_species"] != self.kf_results["kmerfinder_main_hit"]:
            # discordance in first hits
            self.bioinformaic_concordance = "Discordance"
            if self.rmlst_results["support"] == "100" or self.kf_results["kmerfinder_main_hit"].split(" ")[-1] == "sp.":
                if self.rmlst_results["rmlst_species"] == self.kf_results["other_content_1"]:
                    # but KF 2nd hit agrees and rMLST support is 100% or KF had ambiguous 1st hit
                    self.bioinformaic_concordance = "First hit Discordance"
                    self.concluding_id = self.rmlst_results["rmlst_species"]
        else:
            # if they agree conclude on id
            self.concluding_id = self.rmlst_results["rmlst_species"]

        # contamination present?
        if self.kf_results["kmerfinder_main_hit"] and self.rmlst_results["rmlst_species"]:
            if float(self.kf_results["kmerfinder_main_hit_percentage"]) < KF_CONTAMINATION_MIN or (self.kf_results["other_content_1_query_coverage"] and float(self.kf_results["other_content_1_query_coverage"]) > KF_OTHER_HITS_MAX):
                self.possible_contamination_alert = "KF contamination"

            if int(self.rmlst_results["support"]) < RMLST_CONTAMINATION_MIN:
                if not self.possible_contamination_alert:
                    self.possible_contamination_alert = "RMLST contamination"
                else:
                    self.possible_contamination_alert = "KF+RMLST contamination"
            # if contamination is suspected, and rMLST not high enough to "clear" the suspicion, remove concluded id
            if self.possible_contamination_alert and int(self.rmlst_results["support"]) < RMLST_CLEAR_CONTAMINATION_MIN:
                self.concluding_id = ""

        if hasattr(self, "suspected_species") and self.concluding_id:
            # db supplied
            if self.concluding_id == self.suspected_species or self.concluding_id.split()[0] == self.suspected_species.strip():
                self.suspected_vs_identified = "Same"
            else:
                self.suspected_vs_identified = "Different"

        return

    def evaluate_sequencing(self):
        # thresholds
        MIN_BASES = 100
        MIN_N50 = 15000
        MAX_CONTIG = 500
        MIN_COV = 20

        self.qc_alert = ""
        if not assembly_record.qc_stats["qual_bases_mc"]:
            self.qc_alert = "No report"
        elif int(self.qc_stats["qual_bases_mc"]) < MIN_BASES and float(self.qc_stats["coverage"]) < MIN_COV:
            self.qc_alert = "Low qual bases"
        elif int(self.qc_stats["n50"]) < MIN_N50:
            self.qc_alert = "Low N50"
        elif int(self.qc_stats["no_ctgs_min_500"]) > MAX_CONTIG:
            self.qc_alert = "High contig"

        return

    def evaluate_for_rerun(self):
        self.final = "1"
        # low qual sequencing
        if self.qc_alert:
            # P aeruginosa is allowed to have more contigs, as a treat
            if self.qc_alert == "High contig" and self.rmlst_results["rmlst_species"] == "Pseudomonas aeruginosa":
                self.final = "2"
            else:
                self.final = "0"

        # typing doesn't agree even with the 2nd hit
        if self.bioinformaic_concordance == "Discordance":
            self.final = "0"

        # if rMLST is not conclusive enough
        if self.possible_contamination_alert and not self.concluding_id:
            self.final = "0"

    def print_csv(self, outfile_handle, print_colnames):
        column_names = [
        "sequence_name",
        "sample_id",
        "extraction_batch",
        "bases_sequenced",
        "qual_bases_mc",
        "qual_bases_percentage",
        "reads",
        "qual_reads",
        "qual_reads_percentage",
        "most_common_adapter_count",
        "second_most_common_adapter_count",
        "other_adapters_count",
        "n50",
        "no_ctgs_min_500",
        "longest",
        "assembly_size_bp",
        "coverage",
        "qc_alert",
        "kmerfinder_main_hit",
        "kmerfinder_main_hit_percentage",
        "other_content_1",
        "other_content_1_query_coverage",
        "other_content_2",
        "other_content_2_query_coverage",
        "other_content_3",
        "other_content_3_query_coverage",
        "rmlst_species",
        "rank",
        "support",
        "rmlst_species_hit2",
        "rank_hit2",
        "support_hit2",
        "possible_contamination_alert",
        "bioinformaic_concordance",
        "concluding_id",
        "suspected_species",
        "suspected_vs_identified",
        "final"
        ]
        col_div = "\t"

        if print_colnames:
            print(col_div.join(column_names), file=outfile)

        row = []
        for colname in column_names:
            if hasattr(self, colname):
                row.append(getattr(self, colname))
            elif colname in getattr(self, "qc_stats"):
                row.append(getattr(self, "qc_stats").get(colname))
            elif colname in getattr(self, "kf_results"):
                row.append(getattr(self, "kf_results").get(colname))
            elif colname in getattr(self, "rmlst_results"):
                row.append(getattr(self, "rmlst_results").get(colname))
            else:
                row.append("NULL")
        print(col_div.join(row), file=outfile)

def dir_arg_check(param):
    if param is None or not os.path.exists(param) or not os.path.isdir(param):
        sys.exit(f"Error: {param} not found")
    return

def find_assemblies(seq_run_dir):
    assembly_dir = os.path.join(seq_run_dir, "Assemblies")
    dir_arg_check(assembly_dir)
    os.chdir(assembly_dir)
    assembly_names = glob("*.fa")
    return assembly_names


parser = argparse.ArgumentParser(
    description='Parse FoodQCPipeline, KmerFinder and rMLST output for twiw'
)
parser.add_argument(
    '-s',
    dest='seq_run_dir',
    required=True,
    help='Sequencing run folder, full path'
)
parser.add_argument(
    '-k',
    dest='kf_dir',
    required=True,
    help='KmerFinder results folder, full path'
)
parser.add_argument(
    '-r',
    dest='rmlst_dir',
    required=True,
    help='rMLST results folder, full path'
)
parser.add_argument(
    '-i',
    dest='assembly_filename',
    help='Optional assembly filename, for processing a single record'
)
parser.add_argument(
    '-c',
    dest='mysql_configfile',
    help='Optional mysql config file for connecting to db'
)
parser.add_argument(
    '-o',
    dest='ofix',
    required=True,
    help='Output path for csv'
)
parser.add_argument(
    '-n',
    dest='header',
    action='store_false',
    help='Do not print column names'
)
args = parser.parse_args()

dir_arg_check(args.seq_run_dir)
dir_arg_check(args.kf_dir)
dir_arg_check(args.rmlst_dir)
online_access = False
if args.mysql_configfile is not None:
    if os.path.exists(args.mysql_configfile):
        online_access = True
    else:
        print("WARNING: mysql database access config file not found", file=sys.stderr)

# base it on the assembly names in the seq_run folder
assemblies = []
if args.assembly_filename is None:
    assemblies = find_assemblies(args.seq_run_dir)
else:
    assemblies.append(os.path.basename(args.assembly_filename))

header = args.header
qc_folder = os.path.join(args.seq_run_dir, "QC")
try:
    outfile = open(args.ofix, "w")
except OSError as e:
    print(f"Error: {args.ofix} not found", file=sys.stderr)
    outfile = sys.stdout

for assembly in assemblies:
    # init class
    assembly_record = SEQUENCING_RUN_PARSER(assembly)

    # parse and gather data
    assembly_record.parse_qc_txt(qc_folder)
    assembly_record.parse_kf_spa(args.kf_dir)
    assembly_record.parse_rmlst_txt(args.rmlst_dir)

    # get suspected taxonomy and extraction batch no
    if online_access:
        twiw_db_acc = DATABASE_ACCESS(args.mysql_configfile)
        twiw_db_acc.query_table("sample", "sample_id", assembly_record.sample_id, ["suspected_genus_validated", "suspected_species_validated"])
        twiw_db_acc.query_table("seq_dna_extraction_1", "sequence_name", assembly_record.sequence_name, ["extraction_batch"])
        twiw_db_acc.close_connection()
        assembly_record.fill_column("suspected_species", twiw_db_acc.query_responses['sample'])
        assembly_record.fill_column("extraction_batch", twiw_db_acc.query_responses['seq_dna_extraction_1'])

    # evaluate parsed results to set flags
    assembly_record.evaluate_typing()
    assembly_record.evaluate_sequencing()

    # evaluate based on flags
    # rerun
    assembly_record.evaluate_for_rerun()
    # don't print column names for the following samples
    assembly_record.print_csv(outfile, header)
    header = False

outfile.close()
