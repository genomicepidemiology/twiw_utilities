#!/usr/bin/env python3

import sys
import os
from glob import glob
import mysql.connector
from mysql.connector import errorcode
import argparse
import configparser
sys.path.append(os.path.dirname(sys.argv[0]))
from mysql_access import DATABASE_ACCESS


def dir_arg_check(param):
    if param is None or not os.path.exists(param) or not os.path.isdir(param):
        sys.exit(f"Error: {param} not found")
    return

def find_newicks(phylo_run_dir):
    os.chdir(phylo_run_dir)
    newick_names = glob("*.newick")
    return newick_names


parser = argparse.ArgumentParser(
    description='Create tables and insert phylogeny results'
)
parser.add_argument(
    '-s',
    dest='phylo_run_dir',
    required=True,
    help='Phylogeny run folder, full path'
)
parser.add_argument(
    '-i',
    dest='newick_filename',
    help='Optional newick filename'
)
parser.add_argument(
    '-c',
    dest='mysql_configfile',
    required=True,
    help='Mysql config file for connecting to db'
)
args = parser.parse_args()

dir_arg_check(args.phylo_run_dir)

# find the nwk files
newicks = []
if args.newick_filename is None:
    newicks = find_newicks(args.phylo_run_dir)
else:
    newicks.append(os.path.basename(args.newick_filename))

# collect data to be inserted
tree_inserts = []
template_samples = {}
for nwk in newicks:
    template_name = nwk.rsplit("_",3)[0]
    nwk_path = os.path.join(args.phylo_run_dir, nwk)
    nwk_cont = []
    with open(nwk_path, "r") as fp:
        for line in fp:
            nwk_cont.append(line.strip())
    tree_inserts.append((template_name, "".join(nwk_cont)))
    nwk_samples = []
    with open(f"{nwk_path}.lst", "r") as fp:
        for line in fp:
            if not line.startswith("template"):
                nwk_samples.append(line.strip())
    template_samples[template_name] = nwk_samples

# connect to db and create tables if needed
twiw_db_acc = DATABASE_ACCESS(args.mysql_configfile)
trees_table_stm = ('''CREATE TABLE IF NOT EXISTS `phylo_trees`
    (`tree_id` int NOT NULL AUTO_INCREMENT,
    `template_name` VARCHAR(200),
    `ctime` datetime DEFAULT CURRENT_TIMESTAMP,
    `newick` text,
    PRIMARY KEY (`tree_id`),
    UNIQUE KEY (`template_name`)
    ) ENGINE=InnoDB;''')
twiw_db_acc.create_table(trees_table_stm)

samples_table_stm = ('''CREATE TABLE IF NOT EXISTS `phylo_samples`
    (`db_id` int NOT NULL AUTO_INCREMENT,
    `sample_id` int NOT NULL,
    `tree_id` int NOT NULL,
    PRIMARY KEY (`db_id`),
    UNIQUE KEY (`sample_id`, `tree_id`),
    CONSTRAINT `phylo_samples_ibfk_1` FOREIGN KEY (`sample_id`)
        REFERENCES `sample` (`sample_id`),
    CONSTRAINT `phylo_samples_ibfk_2` FOREIGN KEY (`tree_id`)
        REFERENCES `phylo_trees` (`tree_id`) ON DELETE CASCADE
    ) ENGINE=InnoDB;''')
twiw_db_acc.create_table(samples_table_stm)

# insert newicks
twiw_db_acc.replace_multiple_rows("phylo_trees", tree_inserts, ["template_name", "newick"])

# query for latest tree_id-s
twiw_db_acc.query_complex_table("phylo_trees", ["template_name", "tree_id"], "(template_name,ctime) IN ( SELECT template_name, MAX(ctime) FROM phylo_trees GROUP BY template_name )")

# pair sample_ids to tree_ids
sample_insert = []
for template in template_samples.keys():
    twiw_db_acc.query_table("phylo_trees", "template_name", template, ["tree_id"])
    for sampleid in template_samples.get(template):
        sample_insert.append((sampleid, twiw_db_acc.query_responses['phylo_trees']["tree_id"]))

# insert sample - tree_id pairs
twiw_db_acc.insert_multiple_rows("phylo_samples", sample_insert, ["sample_id", "tree_id"])

twiw_db_acc.close_connection()
