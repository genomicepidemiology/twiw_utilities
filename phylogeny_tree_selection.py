#!/usr/bin/env python3

import sys
import os
from glob import glob
import shutil
import sqlite3
import argparse

def dir_arg_check(param):
    if param is None or not os.path.exists(param) or not os.path.isdir(param):
        sys.exit(f"Error: {param} not found")
    return

def locate_copy_newick(input_dir, output_dir, template_name):
    os.chdir(input_dir)
    name_pattern = f"{template_name}_*.newick"
    newick_filename = glob(name_pattern)
    try:
        shutil.copy(newick_filename[0], output_dir)
    except:
        print(f"{newick_filename} not copied", file=sys.stderr)

parser = argparse.ArgumentParser(
    description='Reduce the number of trees for each species, retaining coverage'
)
parser.add_argument(
    '-i',
    dest='phylo_run_dir',
    required=True,
    help='Phylogeny run folder'
)
parser.add_argument(
    '-o',
    dest='phylo_out_dir',
    required=True,
    help='Output folder, full path'
)
parser.add_argument(
    '-db',
    dest="database",
    required=True,
    help="Database, absolute path")
parser.add_argument(
    '-p',
    dest="dry_run",
    action="store_true",
    help="Just print the templates")
args = parser.parse_args()

dir_arg_check(args.phylo_run_dir)
dir_arg_check(args.phylo_out_dir)

# access database
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
cur = conn.cursor()

unique_spp = {}
cur.execute("select distinct(species) from templates where qc_pass = 1;")
distinct_spp_sv = cur.fetchall()
if distinct_spp_sv:
    for spp_row in distinct_spp_sv:
        spp = spp_row[0]
        cur.execute("select distinct(sra_id) from templates where species=? and qc_pass = 1;", (spp,))
        samples_sv = [x[0] for x in cur.fetchall()]
        cur.execute("select template,count(*) as sample_no from templates where qc_pass=1 and species=? group by template order by count(template) desc;", (spp,))
        template_counts_sv = cur.fetchall()
        for c_row in template_counts_sv:
            template_name = c_row[0]
            cur.execute("select nw_path from trees where template=?;", (template_name,))
            if cur.fetchall():
                cur.execute("select sra_id from templates where template=? and qc_pass = 1;", (template_name,))
                template_samples_sv = cur.fetchall()
                if unique_spp.get(spp) is None:
                    unique_spp[spp] = []
                    # this template has all samples
                    if len(samples_sv) == len(template_samples_sv):
                        unique_spp[spp].append(template_name)
                        break
                novel_sample = False
                for s_row in template_samples_sv:
                    try:
                        samples_sv.remove(s_row[0])
                        novel_sample = True
                    except ValueError:
                        pass
                if novel_sample:
                    unique_spp[spp].append(template_name)
                if not samples_sv:
                    break

conn.close()

for species in unique_spp.keys():
    for tmpl in unique_spp.get(species):
        print(species, tmpl, sep="\t", file=sys.stdout)
        if not args.dry_run:
            locate_copy_newick(args.phylo_run_dir, args.phylo_out_dir, tmpl)
