#!/usr/bin/env python3

import os
import sys
import argparse
import json

parser = argparse.ArgumentParser(
    description='VirulenceFinder json to csv line')
parser.add_argument(
    '-i',
    dest="json_path",
    help='Path to json')
parser.add_argument(
    '-a',
    dest="attributes",
    required=True,
    help="Comma separted result attributes fx identity, template_length"
)
parser.add_argument(
    '-s',
    dest="sampleinfo",
    required=True,
    help="Comma separted sample name and sampleid"
)
args = parser.parse_args()

cols = []
if args.attributes:
    cols = args.attributes.strip().split(",")
if os.path.exists(args.json_path):
    results = json.load(open(args.json_path, "r"))
    # d['virulencefinder']['results']['Listeria']['listeria']['AgrA:1:NC_003210.1']['template_length']
    for finder in results.values():
        for organism in finder['results'].values():
            for finder_db in organism.keys():
                if type(organism[finder_db]) is str:
                    result_values = [args.sampleinfo,finder_db]
                    result_values.extend(["NULL" for x in cols])
                    print(",".join(result_values))
                else:
                    for hit in organism[finder_db].values():
                        result_values = [args.sampleinfo,finder_db]
                        # keep order in cols
                        for att in cols:
                            if att in hit.keys():
                                if type(hit.get(att)) is str:
                                    # wrap strings in double quotes
                                    result_values.append(f'"{hit.get(att).strip()}"')
                                else:
                                    result_values.append(str(hit.get(att)))
                        print(",".join(result_values))

