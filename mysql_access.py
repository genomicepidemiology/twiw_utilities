#!/usr/bin/env python3.7

import sys
import mysql.connector
from mysql.connector import errorcode
import configparser

class DATABASE_ACCESS:
    """Connect to project database and perform transactions"""
    def __init__(self, config_filepath):
        self.query_responses = {}
        # get connection parameters from config
        config = configparser.ConfigParser()
        config.read(config_filepath)

        try:
            self.conn = mysql.connector.connect(host=config['MYSQL']['Host'], database=config['MYSQL']['Database'],
                                  user=config['USER']['User'],
                                  password=config['USER']['Password'])

        except mysql.connector.Error as e:
            if e.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Access denied, check usr or pwd", file=sys.stderr)
            elif e.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist", file=sys.stderr)
            else:
                print(str(e), file=sys.stderr)

    def create_table(self, table_definition):
        cur = self.conn.cursor()
        try:
            cur.execute(table_definition)
            print("Table created", file=sys.stderr)
            cur.close()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_CANT_CREATE_TABLE:
                print("Cant create table", file=sys.stderr)
            elif err.errno == errorcode.ER_BAD_TABLE_ERROR:
                print("Table definition is wrong", file=sys.stderr)
            else:
                print(err.msg)

    def insert_multiple_rows(self, table, data_sequence, to_cols):
        cur = self.conn.cursor()
        cols = ", ".join(to_cols)
        value_placeholders = ", ".join(["%s" for x in range(len(to_cols))])
        statement = f"INSERT INTO {table} ({cols}) VALUES ({value_placeholders});"
        cur.executemany(statement, data_sequence)
        self.conn.commit()
        cur.close()
        return

    def replace_multiple_rows(self, table, data_sequence, to_cols):
        cur = self.conn.cursor()
        cols = ", ".join(to_cols)
        value_placeholders = ", ".join(["%s" for x in range(len(to_cols))])
        statement = f"REPLACE INTO {table} ({cols}) VALUES ({value_placeholders});"
        cur.executemany(statement, data_sequence)
        self.conn.commit()
        cur.close()
        return

    def query_table(self, table, where_col, where_value, result_cols, free_text=""):
        cur = self.conn.cursor(buffered=True, dictionary=True)
        results = ", ".join(result_cols)
        query = f"SELECT {results} FROM {table} WHERE {where_col}=\"{where_value}\" {free_text};"
        cur.execute(query)
        self.query_responses[table] = cur.fetchone()
        cur.close()
        return

    def query_complex_table(self, table, result_cols, where_text):
        cur = self.conn.cursor(buffered=True, dictionary=True)
        results = ", ".join(result_cols)
        query = f"SELECT {results} FROM {table} WHERE {where_text};"
        cur.execute(query)
        self.query_responses[table] = cur.fetchall()
        cur.close()
        return

    def close_connection(self):
        self.conn.close()
        return
