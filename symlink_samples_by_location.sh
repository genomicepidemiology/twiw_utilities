#!/usr/bin/bash

declare -A FOLDER_STRUCT=( ["Assemblies"]="Assemblies" ["Raw_data"]="Raw_data" ["Trimmed"]="Trim_data" )
ORIGIN_LOOKUP="/home/projects/cge/data/projects/9000/1209/creating_archives/lookup_tables/lookup_origin_label.tsv"
SYMDATA_DIR="/home/projects/cge/data/projects/9000/1209/data_by_location"
DATA_DIR=$1

if [ -z "$DATA_DIR" ];
then
    echo "symlink_samples_by_location.sh <full path to sequencing data directory>";
else
    for DATA_TYPE in Assemblies Raw_data Trimmed ;
    do
        if [[ -e ${DATA_DIR}/${DATA_TYPE} ]];
        then
            for F in `find ${DATA_DIR}/${DATA_TYPE} -mindepth 1 -maxdepth 1 -type f`;
            do
                # determine sample origin
                FILENAME=$(basename $F);
                SAMPLE_ORIGIN=$(echo $FILENAME | cut -d"_" -f 6-7);
                if [ -z "$SAMPLE_ORIGIN" ];
                then
                    echo "$F not TWIW";
                else
                    ORIGIN_COUNTRY=$(grep $SAMPLE_ORIGIN $ORIGIN_LOOKUP | cut -f 2);
                    ORIGIN_CITY=$(grep $SAMPLE_ORIGIN $ORIGIN_LOOKUP | cut -f 3);
                    if [ -z "$ORIGIN_COUNTRY" ];
                    then
                        echo "$F not in lookup";
                    else
                        # create folders if necessary
                        COUNTRY_FOLDER="${SYMDATA_DIR}/${ORIGIN_COUNTRY}";
                        if [[ ! -e ${COUNTRY_FOLDER} ]];
                        then
                            mkdir ${COUNTRY_FOLDER};
                        fi
                        CITY_FOLDER="${COUNTRY_FOLDER}/${ORIGIN_CITY}";
                        if [[ ! -e ${CITY_FOLDER} ]];
                        then
                            mkdir ${CITY_FOLDER};
                        fi
                        if [[ ! -e ${CITY_FOLDER}/${FOLDER_STRUCT[$DATA_TYPE]} ]];
                        then
                            mkdir ${CITY_FOLDER}/${FOLDER_STRUCT[$DATA_TYPE]};
                        fi
                        # symlink file
                        echo -e "${FILENAME}\t${SAMPLE_ORIGIN}\t${ORIGIN_COUNTRY}\t${ORIGIN_CITY}";
                        ln -t ${CITY_FOLDER}/${FOLDER_STRUCT[$DATA_TYPE]} -s ${F};
                    fi
                fi
            done
        fi
    done
fi

# for DATA_TYPE in Assemblies Raw_data Trimmed ;
# do
#     if [[ -e ${DATA_DIR}/${DATA_TYPE} ]];
#     then
#         for F in `find ${DATA_DIR}/${DATA_TYPE} -mindepth 1 -maxdepth 1 -type f`;
#         do
#             # determine sample origin
#             FILENAME=$(basename $F);
#             SAMPLE_ORIGIN=$(echo $FILENAME | cut -d"_" -f 6-7);
#             echo $SAMPLE_ORIGIN;
#             if [ -z "$SAMPLE_ORIGIN" ];
#             then
#                 echo "$F not TWIW";
#             else
#                 ORIGIN_COUNTRY=$(grep $SAMPLE_ORIGIN $ORIGIN_LOOKUP | cut -f 2);
#                 ORIGIN_CITY=$(grep $SAMPLE_ORIGIN $ORIGIN_LOOKUP | cut -f 3);
#                 if [ -z "$ORIGIN_COUNTRY" ];
#                 then
#                     echo "$F not in lookup";
#                 else
#                     echo -e "${FILENAME}\t${SAMPLE_ORIGIN}\t${ORIGIN_COUNTRY}\t${ORIGIN_CITY}";
#                 fi
#             fi
#         done
#     fi
# done
