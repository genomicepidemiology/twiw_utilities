# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 12:09:02 2021

@author: bavja
"""

import sys
import os
import time
import argparse
import pandas as pd
import json
import openpyxl
from openpyxl.styles import Alignment

def main():

    # Get args
    args = parse_arguments()

    # Check args and get list of json files
    json_files = check_arguments(args)

    # Get input files
    samples, all_mlst, all_schemes = parse_json_files(args, json_files)

    # Create output files
    create_summary(samples, all_mlst, all_schemes, args)

    # Print success
    print("The following summary workbook has been created: %s." % (args.o + ".xlsx"))

    return()


def parse_arguments():

    # Description
    service_description = "Tool multiple results created with MLST \
                            results into a single spreadsheet, using the data.json \
                            files that MLST produces. The script takes \
                            a folder as input and looks one level down into subfolders \
                            of the input directory for the data.json files, \
                            using the folder name as sample name."

    # Create parser
    arg_parser = argparse.ArgumentParser(description=service_description, prog = "mlst_summary.py")

    # Input dir
    arg_parser.add_argument('-i', metavar='INPUT_DIR',
                            type=str,
                            help='Directory containing MLST output folders',
                            required = True)

    # Output file name
    arg_parser.add_argument('-o', metavar='OUTPUT_FILENAME',
                            type=str,
                            help='Name of the output summary file',
                            required = False, default = "MLST_Summary")

    # Take sample name from json file
    arg_parser.add_argument('--jname',
                            help='Activate to retrieve sample name from Json files',
                            action="store_true",
                            required = False, default = False)

    arg_parser.add_argument('--dname',
                            help='Activate to retrieve sample name from input directories files',
                            action="store_true",
                            required = False, default = True)



    args = arg_parser.parse_args()

    return(args)


def check_arguments(args):

    # Check args.i
    args.i = os.path.abspath(args.i).replace("\\", "/") + "/"
    if not os.path.exists(args.i):

        print("Error: The specified input directory does not exist.")
        sys.exit(1)


    # Check args.o
    if len(args.o.split(".")) > 1:
        args.o = args.o.split(".")[0]

    # Find all subdirectories in folder
    json_files = dict()
    for path in os.listdir(args.i):

        if os.path.isdir(args.i + path):

            for file in os.listdir(args.i + path):

                if file == "data.json":
                    json_files[args.i + path + "/" + file] = path


    # Check if dirs contains files
    if not len(json_files) > 0:
        print("Error: No data.json files found in any of the subdirectories of input directory: %s" % args.i)
        sys.exit()


    return(json_files)


def parse_json_files(args, json_files):

    # Parse json files
    samples = list()
    all_mlst = set()
    sample_name = ""
    all_schemes = set()
    for file in json_files:

        # Open file and load with json
        fh = open(file,)
        data = json.load(fh)

        # Make sure data contains mlst
        if not data["mlst"]:
            print("Error! Data from file %s does not contain MLST." % file, flush = True)
            continue

        # Check to see where we are getting the sample name from (directory or json file)
        if args.jname:
            sample_name = os.path.basename(data["mlst"]["user_input"]["filename"][1].split('.')[0])
        elif args.dname:
            sample_name = json_files[file]

        # Get data for sample, create sample object with data
        sample = Sample(sample_name)
        sample.add_results(data["mlst"]["results"]["allele_profile"])
        sample.add_detailed_results(data["mlst"]["results"]["allele_profile"])

        # Scheme and other info
        sample.species = data["mlst"]["user_input"]["species"]
        sample.sequence_type = data["mlst"]["results"]["sequence_type"]
        sample.nearest_sts = data["mlst"]["results"]["nearest_sts"]

        # Make a list of samples
        samples.append(sample)

        # Update the set of all plasmids
        all_schemes.add(str(sample.scheme))

    # Check how many samples we have parsed
    if not len(samples) > 0:
        print("Error: No viable data.json output files found in specified input directory: %s" % args.i)
        sys.exit(1)

    return(samples, all_mlst, all_schemes)



def create_summary(samples, all_mlst, all_schemes, args):

    # Define colour scheme
    greenfill = openpyxl.styles.PatternFill(fill_type='solid',
                                        start_color='6EBE50',
                                        end_color='6EBE50')
    lightgreenfill = openpyxl.styles.PatternFill(fill_type='solid',
                                             start_color='BEDCBE',
                                             end_color='BEDCBE')
    greyfill = openpyxl.styles.PatternFill(fill_type='solid',
                                           start_color='9D9D9D',
                                           end_color='9D9D9D')
    redfill = openpyxl.styles.PatternFill(fill_type='solid',
                                      start_color='BF2026',
                                      end_color='BF2026')
    bluefill = openpyxl.styles.PatternFill(fill_type='solid',
                                               start_color='3956A6',
                                               end_color='3956A6')
    lightbluefill = openpyxl.styles.PatternFill(fill_type='solid',
                                               start_color='7488c0',
                                               end_color='7488c0')
    font_bold_white = openpyxl.styles.Font(bold=True, color='ffffff')
    font_bold_black = openpyxl.styles.Font(bold=True, color='000000')

    # Create workbook
    summary_wb = openpyxl.Workbook()

    # Typing results sheet
    res_sheet = summary_wb.active
    res_sheet.title = "mlst_results"

    max_name_length = max([len(sample.name) for sample in samples])

    # Go through all schemes
    i = 2
    for scheme in all_schemes:

        # Get all allele names
        for sample in samples:
            if sample.scheme == scheme:
                allele_names = sample.alleles

        # Header for scheme
        res_sheet.cell(row=i, column=1).value = str(scheme)
        res_sheet.cell(row=i, column=1).fill = bluefill
        res_sheet.cell(row=i, column=1).font = font_bold_white
        i+=1
        header = ["sample", "species"] + allele_names + ["sequence_type", "nearest_sts"]
        max_name_length = max([len(sample.name) for sample in samples])

        for k, item in enumerate(header, start = 1):
            res_sheet.cell(row=i, column=k).value = str(item)
            res_sheet.cell(row=i, column=k).fill = bluefill
            res_sheet.cell(row=i, column=k).font = font_bold_white
        i+=1

        # Write results overview
        for sample in samples:
            if sample.scheme == scheme:
                res_sheet.cell(row=i,column=1).value = str(sample.name)
                res_sheet.cell(row=i,column=2).value = str(sample.species)
                for j, allele in enumerate(allele_names, start = 3):
                    res_sheet.cell(row=i,column=j).value = str(sample.detailed_output[allele][2])
                    res_sheet.cell(row=i,column=j).fill = sample.detailed_output[allele][-1]
                res_sheet.cell(row=i,column=j+1).value = str(sample.sequence_type)
                res_sheet.cell(row=i,column=j+2).value = str(sample.nearest_sts)
                i+=1

        i += 1


    # Write detailed results
    sheet_name = "Detailed_overview"
    summary_wb.create_sheet(sheet_name)
    res_sheet = summary_wb[sheet_name]

    # Write results overview
    i = 1
    header = ["sample", "locus", "align_len", "allele", "allele_name", "coverage", "gaps", "identity", "sbj_len"]
    for k, item in enumerate(header, start = 1):
        res_sheet.cell(row=i, column=k).value = str(item)
        res_sheet.cell(row=i, column=k).fill = bluefill
        res_sheet.cell(row=i, column=k).font = font_bold_white
    i+=1
    for sample in samples:
        for allele in sample.detailed_output:
            res_sheet.cell(row=i,column=1).value = str(sample.name)
            res_sheet.cell(row=i,column=2).value = str(allele)
            res_sheet.cell(row=i,column=1).fill = sample.detailed_output[allele][-1]
            res_sheet.cell(row=i,column=2).fill = sample.detailed_output[allele][-1]
            for j, data in enumerate(sample.detailed_output[allele][:-1], start = 3):
                res_sheet.cell(row=i,column=j).value = str(data)
                res_sheet.cell(row=i,column=j).fill = sample.detailed_output[allele][-1]
            i+=1

    i += 1

    # Now create output guide sheet - colours
    summary_wb.create_sheet("Output_explanation")
    output_guide_sheet = summary_wb["Output_explanation"]
    output_guide_sheet.cell(row = 1, column = 1).value = "The following is an explanation of the colours indications in the table:"
    output_guide_sheet.cell(row = 1, column = 1).font = font_bold_black

    output_guide_sheet.cell(row = 3, column = 2).fill = greenfill
    output_guide_sheet.cell(row = 3, column = 3).value = "Green colour indicates a perfect match, the % Identity will be 100, the allele length will equal the HSP length and the number of gaps will be 0."
    output_guide_sheet.cell(row = 4, column = 2).fill = greyfill
    output_guide_sheet.cell(row = 4, column = 3).value = "The grey colour indicates a warning due to an imperfect match."
    output_guide_sheet.cell(row = 5, column = 2).fill = redfill
    output_guide_sheet.cell(row = 5, column = 3).value = "The red colour indicates no match at all."

    # Guide sheet - terms explanation
    output_guide_sheet.column_dimensions['B'].width = len("Query/HSP Length:") + 2
    output_guide_sheet.cell(row = 7, column = 1).value = "The following is an explanation of the terms used above:"
    output_guide_sheet.cell(row = 7, column = 1).font = font_bold_black

    output_guide_sheet.cell(row = 9, column = 2).value = "% Identity:"
    output_guide_sheet.cell(row = 9, column = 2).font = font_bold_black
    output_guide_sheet.cell(row = 9, column = 3).value = "Percentage of nucleotides that are identical between the best matching MLST allele "
    output_guide_sheet.cell(row = 10, column = 3).value = "in the database and the corresponding sequence in the plasmid."

    output_guide_sheet.cell(row = 12, column = 2).value = "align_len:"
    output_guide_sheet.cell(row = 12, column = 2).font = font_bold_black
    output_guide_sheet.cell(row = 12, column = 3).value = " Length of the alignment between the best matching MLST allele in the database and the corresponding sequence in the plasmid, "
    output_guide_sheet.cell(row = 13, column = 3).value = "also called the high-scoring segment pair (HSP)."
    output_guide_sheet.cell(row = 14, column = 3).value = "A perfect alignment is 100%, but must also cover the entire length of the plasmid in the database."

    output_guide_sheet.cell(row = 15, column = 2).value = "allele_len:"
    output_guide_sheet.cell(row = 15, column = 2).font = font_bold_black
    output_guide_sheet.cell(row = 15, column = 3).value = "Length of the best matching MLST allele in the database."

    output_guide_sheet.cell(row = 16, column = 2).value = "allele_name"
    output_guide_sheet.cell(row = 16, column = 2).font = font_bold_black
    output_guide_sheet.cell(row = 16, column = 3).value = "Name of the best matching MLST allele."

    output_guide_sheet.cell(row = 17, column = 2).value = "Gaps:"
    output_guide_sheet.cell(row = 17, column = 2).font = font_bold_black
    output_guide_sheet.cell(row = 17, column = 3).value = "Number of gaps in the HSP."

    output_guide_sheet.cell(row = 18, column = 2).value = "Position in contig:"
    output_guide_sheet.cell(row = 18, column = 2).font = font_bold_black
    output_guide_sheet.cell(row = 18, column = 3).value = "Starting position of the found gene in the contig."

    output_guide_sheet.cell(row = 19, column = 2).value = "Accession:"
    output_guide_sheet.cell(row = 19, column = 2).font = font_bold_black
    output_guide_sheet.cell(row = 19, column = 3).value = "Reference accession number of the gene in the database."

    # Save the results to file
    # fn = args.o + ".xlsx"
    fn = args.o
    try:
        summary_wb.save(fn)
    except PermissionError as e:
        print("Error: Unable to write to file: %s. Please make sure the file isn't open in when running script." % fn, flush = True)
        sys.exit(1)
    return()

class Sample:

    def __init__(self, sample_name):

        # Define colors for results
        self.redfill = openpyxl.styles.PatternFill(fill_type='solid',
                                  start_color='BF2026',
                                  end_color='BF2026')
        self.greyfill = openpyxl.styles.PatternFill(fill_type='solid',
                                               start_color='9D9D9D',
                                               end_color='9D9D9D')
        self.lightgreenfill = openpyxl.styles.PatternFill(fill_type='solid',
                                                     start_color='BEDCBE',
                                                     end_color='BEDCBE')
        self.greenfill = openpyxl.styles.PatternFill(fill_type='solid',
                                                start_color='6EBE50',
                                                end_color='6EBE50')

        # Basic data
        self.name = sample_name
        self.alleles = list()
        self.allele_list = list()
        self.detailed_output = dict()

        # Other data
        self.scheme = ""
        self.profile = ""
        self.sequence_type = ""
        self.nearest_sts = ""


    def add_results(self, data):
        ''' Stores the found genes within database, and figures out which color to display '''

        # Go through plasmid data and fill out
        for allele in data:

            # Save specific gene names
            self.alleles.append(allele)

            # Get the plasmid name
            allele = data[allele]["allele_name"]

        return()

    def add_detailed_results(self, data):
        ''' Stores detailed results'''

        # If data is empty (error in Json file)
        if len(data) == 0:
            return()

        # Go through gene matches, store all results re. alignment
        for allele in data:

            self.allele_list.append(allele)

            if not allele in self.detailed_output:

                self.detailed_output[allele] = list()
                self.detailed_output[allele].append(data[allele]["align_len"])
                self.detailed_output[allele].append(data[allele]["allele"])
                self.detailed_output[allele].append(data[allele]["allele_name"])
                self.detailed_output[allele].append(data[allele]["coverage"])
                self.detailed_output[allele].append(data[allele]["gaps"])
                self.detailed_output[allele].append(data[allele]["identity"])
                self.detailed_output[allele].append(data[allele]["sbj_len"])

                # Test length
                equal_length = (data[allele]["align_len"] == data[allele]["sbj_len"])


                if data[allele]["identity"] == 100 and data[allele]["gaps"] == 0 and equal_length:
                    self.detailed_output[allele].append(self.greenfill)
                elif data[allele]["allele_name"]  == "No hit found":
                    self.detailed_output[allele].append(self.redfill)
                else:
                    self.detailed_output[allele].append(self.greyfill)


            else:

                # Unseen so far
                print("Error: The same allele was found twice in a Json file.")
                continue


if __name__ == "__main__":

    main()
