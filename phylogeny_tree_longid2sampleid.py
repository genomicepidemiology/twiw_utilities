#!/usr/bin/env python3

import argparse
import re
from ete3 import Tree

# Parse command line options
parser = argparse.ArgumentParser(
    description='Shorten taxa ids in tree, \d{3} or [A-Z]{3}_[A-Z]{3}_([A-Z]{3}_)*\d{3}')
parser.add_argument(
   '-o',
   dest="ofix",
   required=True,
   help='Output path')
parser.add_argument(
    '-r',
    dest="pattern",
    required=True,
    help='Regexp pattern to keep')
parser.add_argument(
    '-t',
    dest="treefile",
    required=True,
    help='Input phylogenic tree')
parser.add_argument(
    '-p',
    dest="print_taxaid",
    action="store_true",
    help='Print taxa')
parser.add_argument(
    '-l',
    dest="print_taxaid_list",
    action="store_true",
    help='Print taxa to file')
args = parser.parse_args()

# load the tree to modify
tree = Tree(args.treefile)

if args.print_taxaid_list:
    outlist = open("{}.lst".format(args.ofix.rsplit(".", 1)[0]), "w")

# shorten taxaid by pattern
#print(args.pattern)
comp_p = re.compile(args.pattern)
for node in tree.traverse():
    if node.name:
        node.name = node.name.split("*")[-1]
        match = comp_p.findall(node.name)
        if match:
            node.name = match[0]
        if args.print_taxaid:
            print(node.name)
        elif args.print_taxaid_list:
            print(node.name, file=outlist)

# save tree
tree.write(format=0, outfile=args.ofix)

if args.print_taxaid_list:
    outlist.close()
